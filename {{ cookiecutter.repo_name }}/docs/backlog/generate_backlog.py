# -*- coding: utf-8 -*-

"""
Forge an Excel PMO file from Gitlab project issues

Note:

  When developping USECACHE allow to request Gitlab only one
    :w|!clear; USECACHE=1 python3 make_backlog.py

  To deactivate the cache either remove it `rm .cache` or
    :w|!clear; USECACHE= python3 make_backlog.py

"""

import os
from copy import copy
import pickle
from pathlib import Path
from typing import Any, Dict, Tuple

import gitlab
import openpyxl
import pandas as pd

# Whether to use cached data or not
USECACHE = bool(os.environ.get("USECACHE", False))

# The GitLab instance hostname and port to compute the instance URL
_GITLAB_HOST = os.environ.get("CI_SERVER_HOST", "code.mazars.global")
_GITLAB_PROTOCOL = os.environ.get("CI_SERVER_PROTOCOL", "https")
GITLAB_URL = f"{_GITLAB_PROTOCOL}://{_GITLAB_HOST}"

FILENAME_RAW = ".backlog_raw.xlsx"  # Output file for raw data
FILENAME_STYLE = ".backlog_style.xlsx"  # File for styles
FILENAME_OUTPUT = "backlog_monitoring.xlsx"  # Output file

# The GitLab token to be used (requires at least read-only ADMIN permissions)
GITLAB_TOKEN = os.environ["GITLAB_BACKLOG_GENERATOR_TOKEN"]

# Current project ID in GitLab instance
GITLAB_PROJECT_ID = os.environ["CI_PROJECT_ID"]

# Date format to be used for date conversions
DATE_FORMAT = "%Y-%m-%d"


def get_gitlab_issues_and_users(
    get_all: bool = True,
) -> Tuple[Any, Dict[str, Dict[str, str]]]:

    """
    Returns gitlab issues, and assignee users data.

    This function also implements a cache to avoid requesting gitlab API while
    developing.

    Data structures
    ================

    Issues
    ------

    issues = [<ProjectIssue iid:230>, <ProjectIssue iid:229>, ...]

    Assignees
    ---------

    issues[0].assignee
    {
        'id': 69,
        'name': 'Corentin',
        'username': 'Corentin',
        'state': 'active',
        'avatar_url': 'https://secure.gravatar.com/avatar/ac1...5?s=80&d=identicon',
        'web_url': 'https://code.mazars.global/Corentin'
    }

    gitlab_instance.users.get(id=issue.assignee["id"])._attrs
    {
        'id': 69,
        'name': 'Corentin',
        'username': 'Corentin',
        'state': 'active',
        'avatar_url': 'https://secure.gravatar.com/avatar/ac1...5?s=80&d=identicon',
        'web_url': 'https://code.mazars.global/Corentin',
        'created_at': '2018-07-26T15:05:05.369Z',
        'bio': '',
        'location': None,
        'public_email': '',
        'skype': '',
        'linkedin': '',
        'twitter': '',
        'website_url': '',
        'organization': None,
        'job_title': '',
        'pronouns': None,
        'bot': False,
        'work_information': None,
        'followers': 0,
        'following': 0,
        'local_time': '4:00 PM',
        'last_sign_in_at': '2021-11-30T11:21:13.940Z',
        'confirmed_at': '2018-07-26T15:05:04.976Z',
        'last_activity_on': '2021-12-06',
        'email': 'corentin.muller@mazars.fr',
        'theme_id': 1,
        'color_scheme_id': 1,
        'projects_limit': 40,
        'current_sign_in_at': '2021-12-06T08:39:39.020Z',
        'identities': [],
        'can_create_group': True,
        'can_create_project': True,
        'two_factor_enabled': True,
        'external': False,
        'private_profile': False,
        'commit_email': 'corentin.muller@mazars.fr',
        'is_admin': False,
        'note': None,
        'highest_role': 50,
        'current_sign_in_ip': '10.0.128.103',
        'last_sign_in_ip': '10.0.128.101',
        'sign_in_count': 254
    }

    gitlab_client.users.get(id=issue.assignee["id"]).email
    'corentin.muller@mazars.fr'
    """

    cache = Path(__file__).parent.resolve() / ".cache"

    if cache.is_file() and USECACHE:
        print(f"-> Loading data from cache file (from {cache})")
        issues, users = pickle.load(cache.open("rb"))  # nosec

    else:
        print("-> Loading fresh data from GitLab API")
        gitlab_client = gitlab.Gitlab(GITLAB_URL, private_token=GITLAB_TOKEN)

        project = gitlab_client.projects.get(GITLAB_PROJECT_ID)
        issues = project.issues.list(all=get_all)

        # emails = {}
        # for issue in issues:
        #     if issue.assignee:
        #         user_id = issue.assignee["id"]
        #         emails[user_id] = glab.users.get(id=user_id).email

        assignee_ids = set(
            [issue.assignee["id"] for issue in issues if issue.assignee]
        )
        users = {
            assignee_id: gitlab_client.users.get(id=assignee_id)._attrs
            for assignee_id in assignee_ids
        }

        print(f"-> Dumping fresh data to cache (to {cache})")
        pickle.dump((issues, users), cache.open("wb"))

    return issues, users


def _make_single_issue_dict(users: Dict[str, Dict[str, str]], issue) -> Dict[str, Any]:

    """
    Forge the row namespace from an issue

    This method is specific to Vallourec

    # For consistency, all expected keys must be provided here
    # in the expected order for the template output file.
    # If a value cannot be set at initialization, set it to None
    # and perform computation / assignation later
    """

    backlog_row = {
        "Issue ID": issue.iid,
        "State": issue.state,
        "Title": issue.title,
        "Status": None,
        "Assignee Username": None,
        "Workload Progress": None,
        "Estimated Workload": None,
        "Already Spent": None,
        "ETA": None,
        "Creation Date": None,
        "Labels": ";".join(issue.labels),
        "URL": issue.web_url,
    }

    if issue.assignee:
        backlog_row["Assignee Username"] = issue.assignee["name"]

    to_dt = pd.to_datetime
    backlog_row["Creation Date"] = to_dt(issue.created_at).strftime(
        DATE_FORMAT
    )

    if issue.due_date:
        backlog_row["ETA"] = to_dt(issue.due_date).strftime(DATE_FORMAT)

    # Workloads are provided in seconds
    # Conversion : 1 day = 8 hours
    time_stats = issue.time_stats()
    time_estimate = time_stats["time_estimate"]
    time_spent = time_stats["total_time_spent"]
    if time_spent > 0:
        backlog_row["Already Spent"] = time_spent / (8 * 3600)

    if time_estimate > 0:
        backlog_row["Estimated Workload"] = time_estimate / (8 * 3600)

        # Do not compute progress if time_estimate was not provided
        # Value is 0 if not provided
        backlog_row["Workload Progress"] = time_spent / time_estimate

    return backlog_row


def get_raw_df(issues, users: Dict[str, Dict[str, str]]) -> pd.DataFrame:

    """
    Return a Pandas DataFrame ready to fill an Excel

    Here only the column order could be seen as Vallourec specific
    """

    raw_df = [_make_single_issue_dict(users=users, issue=issue) for issue in issues]

    columns = [
        "Issue ID",
        "State",
        "Title",
        "Status",
        "Assignee Username",
        "Workload Progress",
        "Estimated Workload",
        "Already Spent",
        "ETA",
        "Creation Date",
        "Labels",
        "URL",
    ]
    df_raw = pd.DataFrame(raw_df, columns=columns)

    go_dt = pd.to_datetime
    df_raw.loc[:, "ETA"] = go_dt(df_raw.ETA)
    df_raw.loc[:, "Creation Date"] = go_dt(df_raw.loc[:, "Creation Date"])

    df_raw.sort_values(
        by=[
            "State",
            "Workload Progress",
            "Assignee Username",
            "Status",
        ],
        ascending=[False, False, False, False],
        inplace=True,
    )

    return df_raw


def make_data_style_output_sheet_and_book(
    df_raw: pd.DataFrame,
) -> Tuple[
    openpyxl.worksheet.worksheet.Worksheet,
    openpyxl.worksheet.worksheet.Worksheet,
    openpyxl.worksheet.worksheet.Worksheet,
    openpyxl.workbook.workbook.Workbook,
]:

    """
    Return 3 sheets (raw data, style, new_book.sheet) and the new book

    Note: We save the issues dataframe on a temporary Excel file to easy the
          walk through cells
    """

    # Compute paths
    raw_data = Path(__file__).parent.resolve() / FILENAME_RAW
    style_data = Path(__file__).parent.resolve() / FILENAME_STYLE

    # Dump raw dataset and load created workbook
    df_raw.to_excel(raw_data, sheet_name="raw", header=True, index=False)
    book_raw = openpyxl.load_workbook(raw_data, read_only=False)
    s_raw = book_raw["raw"]

    # Load Excel template to borrow styling
    s_style = openpyxl.load_workbook(style_data, read_only=False).active

    # Create new sheet and remove default "Sheet"
    new_book = openpyxl.workbook.workbook.Workbook()
    n_sheet = new_book.create_sheet(title="Issues Summary")

    # Remove placeholder default sheet if exists
    if "Sheet" in new_book.sheetnames:
        new_book.remove(new_book["Sheet"])

    return s_raw, s_style, n_sheet, new_book


def copy_style_to_raw(
    s_style: openpyxl.worksheet.worksheet.Worksheet,
    s_raw: openpyxl.worksheet.worksheet.Worksheet,
    new_sheet: openpyxl.worksheet.worksheet.Worksheet,
) -> openpyxl.worksheet.worksheet.Worksheet:

    """
    Parse a raw data sheet and apply the Style from the first lines of a style
    Excel sheet, return the result on a new_sheet
    """

    for row in list(s_raw.rows)[:]:
        for cell in list(row)[:]:
            if cell.value:

                row_idx, col_idx = cell.row, cell.column

                # Copy the raw content
                new_sheet.cell(row_idx, col_idx, cell.value)

                # Copy the style content
                style_row_idx = 1 if row_idx == 1 else 2
                s_cell = s_style.cell(style_row_idx, col_idx)

                new_cell = new_sheet.cell(row_idx, col_idx)
                new_cell.font = copy(s_cell.font)
                new_cell.border = copy(s_cell.border)
                new_cell.fill = copy(s_cell.fill)
                new_cell.number_format = copy(s_cell.number_format)
                new_cell.protection = copy(s_cell.protection)
                new_cell.alignment = copy(s_cell.alignment)

            # Copy the cell height and comments for the 4 first lines
            if cell.row <= 4:

                height = s_style.row_dimensions[cell.row].height
                new_sheet.row_dimensions[cell.row].height = height

                if cell.comment:
                    new_sheet.cell(row_idx, col_idx).comment = copy(
                        cell.comment
                    )
    # Copy the column width
    for k, col_d in s_style.column_dimensions.items():
        new_sheet.column_dimensions[k].width = col_d.width

    return new_sheet


def main():

    "main call"

    # Extract data from GitLab API
    issues, users = get_gitlab_issues_and_users()

    # Compute raw df from issues and users data
    df_raw = get_raw_df(issues, users)

    # Apply styles to raw dataframe
    output = make_data_style_output_sheet_and_book(df_raw)
    s_raw, s_style, n_sheet, new_book = output

    n_sheet = copy_style_to_raw(s_style, s_raw, n_sheet)

    # Freeze first row / column to have clipped headers
    n_sheet.freeze_panes = "B2"

    # Save output file
    output_file = Path(__file__).parent.resolve() / FILENAME_OUTPUT
    new_book.save(output_file)


if __name__ == "__main__":
    main()
