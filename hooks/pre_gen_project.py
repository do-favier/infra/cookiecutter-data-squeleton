#%% Import libraries
import subprocess

#%% Define functions
def creation_note():
    print("""

===============================================================================
*** CREATION NOTE ***

This squeleton generates automatically a python project with an ideal
architecture for Python data projects.

Note: To use the legacy template of Cookiecutter in case of v2, you will need 
to explicitly use `-c v1` to select it.

For example:
    cookiecutter -c v1 https://github.com/drivendata/cookiecutter-data-science
===============================================================================

    """)

def init_git():
    subprocess.call(['git', 'init'])
    subprocess.call(['git', 'branch', '-m', 'main'])
    # subprocess.call(['git', 'add', '.'])
    # subprocess.call(['git', 'commit', '-m', 'Initial skeleton commit'])

#%% Main : Execute all the functions
creation_note()